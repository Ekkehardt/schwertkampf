Technik
=======

technik_2015-06
---------------
Tabellarische Übersichten der Langschwert-Techniken nach Hans Thalhofer.


Systeme
-------
plattformunabhängig


Technische Informationen
------------------------
Erstellt mit Texlive 2016, srcartcl, unter Ubuntu 16.04.

Pakete:
	scrlayer-scrpage
	inputenc
	fontenc
	babel
	enumitem
	csquotes
	libertine
	geometry
	eurosym
	booktabs
	hyperref

Rechtliche Informationen
========================
Die Dokumente dürfen ausschließlich für akademische Zwecke genutzt werden. Mitarbeit durch versierte Schwertkämpfer ist ausdrücklich erwünscht.

E. Frank Sandig, 39.09.2016
